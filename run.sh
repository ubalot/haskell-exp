#!/usr/bin/env bash

set -e

PROJ_NAME=haskell-exp

stack build
stack exec "$PROJ_NAME-exe" -- $*
