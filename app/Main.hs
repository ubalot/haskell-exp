module Main where

import Control.Monad (replicateM_, when)
import System.Random (randomRIO)
import System.Directory (doesFileExist)

import Options.Applicative

import Lib

data Sample = Sample
  { debug   :: Bool
  , counter :: Int
  , file    :: String }

sample :: Parser Sample
sample = Sample
      <$> switch
          ( long "debug"
         <> short 'd'
         <> help "Whether to show debug info" )
      <*> option auto
          ( long "counter"
         <> short 'c'
         <> help "How many swears"
         <> showDefault
         <> value 1
         <> metavar "INT" )
      <*> strOption
          ( long "file"
         <> short 'f'
         <> help "File with swears"
         <> showDefault
         <> value ""
         <> metavar "FILENAME" )

main :: IO ()
main = shout =<< execParser opts
  where
    opts = info (sample <**> helper)
      ( fullDesc
     <> progDesc "Print a swears"
     <> header "swear" )

shout :: Sample -> IO ()
shout (Sample debug counter file) = do
  when debug $
    putStrLn $ "Arguments: " ++ "\n"
            ++ "debug = " ++ (show debug) ++ "\n"
            ++ "counter = " ++ (show counter) ++ "\n"
            ++ "file = " ++ (show file) ++ "\n"
  if null file
    then replicateM_ counter (shoutSwear swears)
    else do
      b <- doesFileExist file
      if b
        then do
          contents <- readFile file
          replicateM_ counter (shoutSwear (map show contents))
        else putStrLn $ "File " ++ file ++ " doesn't exists"
  where
    shoutSwear :: [String] -> IO ()
    shoutSwear swears = do
      swear <- pick swears
      n <- randomRIO (1,4)
      putStrLn $ swear ++ (concat (replicate n "!"))

    pick :: [a] -> IO a
    pick xs = fmap (xs !!) $ randomRIO (0, length xs - 1)
